# -*- coding: utf-8 -*-

from odoo import models, fields, api

class GWMarca(models.Model):
    _name = 'gw.marca'
    
    name = fields.Char('Marca',required=True)
    code = fields.Char('Code',required=True)
    
