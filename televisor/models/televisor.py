# -*- coding: utf-8 -*-

from odoo import models, fields, api

class GWTelevisor(models.Model):
    _name = 'gw.televisor'
    _rec_name ="serial"
    
    optionsColor=[
        ('n','Negro'),
        ('b','Blanco'),
        ('n','Gris'),
        ]
    
    serial = fields.Char(
                string="Serial",
                size=10,
                required=True,
                help="El serial del televisor que se encuentra....")
    color = fields.Selection(
                        optionsColor,
                        required=True,
                        default="n")
    pulgadas = fields.Integer(string="Pulgadas")
    peso = fields.Float(string="Peso")
    detalles = fields.Boolean(string="Detalles")
    date = fields.Date(string="Fecha fabricación")
    country_id= fields.Many2one('res.country')
    marca_id= fields.Many2one('gw.marca',required=True)
    modoentrada_ids= fields.Many2many('gw.modo.entrada',string="Modo Entrada",required=True)
    incidencia_ids= fields.One2many('gw.incidencia',"televisor_id",required=True)

    
