# -*- coding: utf-8 -*-

from odoo import models, fields, api

class GWIncidencia(models.Model):
    _name = 'gw.incidencia'
    
    state = fields.Selection(
                            [
                            ('recibido','Recibido'),
                            ('revision','En revisión'),
                            ('resuelto','Resuelto'),
                            ('entregado','Entregado'),
                            ],
                            default="recibido",
                            readonly=True)
    code = fields.Char('code',readonly=True)
    res_partner_id = fields.Many2one('res.partner',required=True)
    date = fields.Date('Fecha Entrega',required=True)
    observacion = fields.Text('Fecha Entrega',required=True)
    dictamen = fields.Html('Dictamen',required=True)
    televisor_id = fields.Many2one('Televisor')
    
    @api.model
    def create(self,vals):
        ComSequence=self.env['ir.sequence']
        vals['code'] = ComSequence.next_by_code('gw.incidencia')
        record=super(GWIncidencia,self).create(vals)
        return record
    
